package org.mamj.util.ref;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RefDataIT {
    private RefData refData;

    @Test
    public void testMappedDataExceptions(RefData refData) {
        // Enterprise data not loaded first
        Assertions.assertThrows(IllegalStateException.class, () -> refData.mapRefDataSet("ms3", null));

        // Empty mapped data
        List<MappedDataObject> mappedData = new ArrayList<>();
        Assertions.assertThrows(IllegalStateException.class, () -> refData.mapRefDataSet("ms3", mappedData));
    }
    */

    @BeforeEach
    public void init(RefData refData) {
        refData.setName("RefDataTest instance");
        List<RefDataObject> enterpriseData = new ArrayList<>();
        enterpriseData.add(RefDataUtil.createRefDataObject("OH", "Ohio", "Something"));
        enterpriseData.add(RefDataUtil.createRefDataObject("MI", "Michigan", "Something"));
        enterpriseData.add(RefDataUtil.createRefDataObject("WI", "Wisconsin", "Something"));
        refData.loadEnterpriseData(enterpriseData);

        List<MappedDataObject> mappedSet1 = new ArrayList<>();
        mappedSet1.add(RefDataUtil.createMappedDataObject("OH", "1", "", ""));
        mappedSet1.add(RefDataUtil.createMappedDataObject("MI", "2", "", ""));
        mappedSet1.add(RefDataUtil.createMappedDataObject("WI", "3", "", ""));
        refData.mapRefDataSet("ms1", mappedSet1);

        List<MappedDataObject> mappedSet2 = new ArrayList<>();
        mappedSet2.add(RefDataUtil.createMappedDataObject("OH", "A", "", ""));
        mappedSet2.add(RefDataUtil.createMappedDataObject("MI", "B", "", ""));
        mappedSet2.add(RefDataUtil.createMappedDataObject("WI", "C", "", ""));
        refData.mapRefDataSet("ms2", mappedSet2);

        this.refData = refData;
    }

    @Test
    public void testEnterpriseDataRetrieval() {
        //initRefData();
        Assertions.assertEquals(this.refData.retrieveEnterpriseDataObject("ms1", "2").getCode(), "MI");
        Assertions.assertEquals(this.refData.retrieveEnterpriseDataObject("ms2", "C").getCode(), "WI");
        Assertions.assertNull(this.refData.retrieveEnterpriseDataObject("ms2", "ZZ"));
    }

    @Test
    public void testRefDataRetrieval() {
        //initRefData();
        Assertions.assertEquals(this.refData.retrieveRefDataObject("ms1", "MI").getCode(), "2");
        Assertions.assertEquals(this.refData.retrieveRefDataObject("ms2", "WI").getCode(), "C");
        Assertions.assertNull(this.refData.retrieveRefDataObject("ms2", "ZZ"));
    }

    /*
    private void initRefData() {
        if (this.refData == null) {
            System.out.println("Null refData");
        }
        List<RefDataObject> enterpriseData = new ArrayList<>();
        enterpriseData.add(RefDataUtil.createRefDataObject("OH", "Ohio", "Something"));
        enterpriseData.add(RefDataUtil.createRefDataObject("MI", "Michigan", "Something"));
        enterpriseData.add(RefDataUtil.createRefDataObject("WI", "Wisconsin", "Something"));
        this.refData.loadEnterpriseData(enterpriseData);

        List<MappedDataObject> mappedSet1 = new ArrayList<>();
        mappedSet1.add(RefDataUtil.createMappedDataObject("OH", "1", "", ""));
        mappedSet1.add(RefDataUtil.createMappedDataObject("MI", "2", "", ""));
        mappedSet1.add(RefDataUtil.createMappedDataObject("WI", "3", "", ""));
        this.refData.mapRefDataSet("ms1", mappedSet1);

        List<MappedDataObject> mappedSet2 = new ArrayList<>();
        mappedSet2.add(RefDataUtil.createMappedDataObject("OH", "A", "", ""));
        mappedSet2.add(RefDataUtil.createMappedDataObject("MI", "B", "", ""));
        mappedSet2.add(RefDataUtil.createMappedDataObject("WI", "C", "", ""));
        this.refData.mapRefDataSet("ms2", mappedSet2);
    }
    */
}
