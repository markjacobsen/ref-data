package org.mamj.util.ref;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;

public class DataStoreImpl implements DataStore {
    private final static Logger logger = LoggerProducer.getLogger(DataStoreImpl.class);

    /**
     * List of enterprise data
     */
    private List<RefDataObject> enterpriseData;

    /**
     * Map w/ key = enterprise data code, and value = refDataObject
     */
    private Map<String, RefDataObject> enterpriseDataMap;

    /**
     * Map w/ key = data set code, and value = ref data set
     */
    private Map<String, Map<String, MappedDataObject>> allRefData = new HashMap<>();

    public void setEnterpriseData(List<RefDataObject> enterpriseData) throws IllegalStateException {
        if ((enterpriseData == null) || (enterpriseData.isEmpty())) {
            throw new IllegalStateException("Enterprise data must have data");
        }

        logger.info("Loading {} enterprise data values", enterpriseData.size());
        this.enterpriseData = enterpriseData;

        this.enterpriseDataMap = new HashMap<>();
        for (RefDataObject item : getEnterprizeData()) {
            addEnterPrizeDataToMap(item);
        }

        logger.debug("Loaded {} enterprise data values", this.enterpriseData.size());
    }

    private void addEnterPrizeDataToMap(RefDataObject item) {
        this.enterpriseDataMap.put(item.getCode(), item);
    }

    public List<RefDataObject> getEnterprizeData() {
        return this.enterpriseData;
    }

    public Map<String, RefDataObject> getEnterpriseDataMap() {
        return this.enterpriseDataMap;
    }

    public Map<String, Map<String, MappedDataObject>> getAllRefData() {
        return this.allRefData;
    }

    public void addRefDataSet(String refDataSetCode, List<MappedDataObject> refDataSet) {
        Map<String, MappedDataObject> refDataMap = new HashMap<>();
        for (MappedDataObject mdo : refDataSet) {
            refDataMap.put(mdo.getCode(), mdo);
        }
        allRefData.put(refDataSetCode, refDataMap);
    }

    public boolean isRefDataSetLoaded(String refDataSetCode) {
        return getAllRefData().containsKey(refDataSetCode);
    }

    public Map<String, MappedDataObject> getRefDataSetMap(String refDataSetCode) {
        return getAllRefData().get(refDataSetCode);
    }
}
