package org.mamj.util.ref;

public class RefDataObject {
    private String code;
    private String name;
    private String info;

    /**
     * Get the Reference Data "code" value (ex: MI)
     * @return
     */
    public String getCode() {
        return code;
    }

    /**
     * Set the Reference Data "code" value (ex: MI)
     * @param code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Get the Reference Data "display name" value (ex: Michigan)
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Set the Reference Data "display name" value (ex: Michigan)
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Get the Reference Data "additional info" value ("26th state in the US")
     * @return
     */
    public String getInfo() {
        return info;
    }

    /**
     * Set the Reference Data "additional info" value ("26th state in the US")
     * @param info
     */
    public void setInfo(String info) {
        this.info = info;
    }
}