package org.mamj.util.ref;

public class RefDataUtil {
    private RefDataUtil() {}

    public static RefDataObject createRefDataObject(String code, String name, String info) {
        RefDataObject rdo = new RefDataObject();
        rdo.setCode(code);
        rdo.setName(name);
        rdo.setInfo(info);
        return rdo;
    }

    public static MappedDataObject createMappedDataObject(String enterpriseCode, String code, String name, String info) {
        MappedDataObject mdo = new MappedDataObject();
        mdo.setEnterpriseCode(enterpriseCode);
        mdo.setCode(code);
        mdo.setName(name);
        mdo.setInfo(info);
        return mdo;
    }
}
