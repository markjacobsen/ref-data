package org.mamj.util.ref;

public class MappedDataObject extends RefDataObject {
    private String enterpriseCode;

    /**
     * Get the Enterprise "code" value this data object maps to (ex: MI)
     * @return
     */
    public String getEnterpriseCode() {
        return enterpriseCode;
    }

    /**
     * Set the Enterprise "code" value this data object maps to (ex: MI)
     * @param code
     */
    public void setEnterpriseCode(String enterpriseCode) {
        this.enterpriseCode = enterpriseCode;
    }
}