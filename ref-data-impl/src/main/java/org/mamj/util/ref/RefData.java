package org.mamj.util.ref;

import java.util.List;

public interface RefData {
    /**
     * Get the frienly name for this instance
     * @return
     */
    String getName();

    /**
     * Set a friendly name for what this instance contains
     * @param name
     */
    void setName(String name);

    /**
     * Retrieve a list of all enterprise data objects
     * @return
     */
    List<RefDataObject> retrieveEnterpriseData();

    /**
     * Load the enterprise data (intentionally forcing load to happen in 1 fell swoop so it's a complete set)
     * @param enterpriseData
     * @throws IllegalStateException If enterprise data is null or empty
     */
    void loadEnterpriseData(List<RefDataObject> enterpriseData) throws IllegalStateException;

    /**
     * Given a specific set of referance data, map it to the enterprise data
     * @param refDataSetCode Unique code for the referance data set
     * @param refDataSet List containing referance data and the enterprise value to map it to
     * @throws IllegalStateException If enterprise data has not been loaded yet or another referance data set of the same name has already been loaded
     */
    void mapRefDataSet(String refDataSetCode, List<MappedDataObject> refDataSet) throws IllegalStateException;

    /**
     * Retun the enterise data for the specified referance data
     * @param refDataSetCode
     * @param refDataCode
     * @return
     */
    RefDataObject retrieveEnterpriseDataObject(String refDataSetCode, String refDataCode);

    /**
     * Return a particular referance data object for a specific enterprise data value
     * @param refDataSetCode
     * @param enterpriseCode
     * @return
     */
    RefDataObject retrieveRefDataObject(String refDataSetCode, String enterpriseCode);
}