package org.mamj.util.ref;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.slf4j.Logger;

@ApplicationScoped
public class RefDataImpl implements RefData {
    private final static Logger logger = LoggerProducer.getLogger(RefDataImpl.class);

    @Inject
    private DataStore ds;

    private String name;

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RefDataObject> retrieveEnterpriseData() {
        return ds.getEnterprizeData();
    }

    public void loadEnterpriseData(List<RefDataObject> enterpriseData) throws IllegalStateException {
        ds.setEnterpriseData(enterpriseData);
    }

    public void mapRefDataSet(String refDataSetCode, List<MappedDataObject> refDataSet) throws IllegalStateException {
        if ((ds.getEnterprizeData() == null) || (ds.getEnterprizeData().isEmpty())) {
            throw new IllegalStateException("Enterprise data has not been loaded yet");
        }

        if ((refDataSet == null) || (refDataSet.isEmpty())) {
            throw new IllegalStateException("Ref Data must have data");
        }
        
        if (ds.isRefDataSetLoaded(refDataSetCode)) {
            logger.warn("Ref Data already loaded for: {}", refDataSetCode);
        } else {
            ds.addRefDataSet(refDataSetCode, refDataSet);
        }
    }

    public RefDataObject retrieveEnterpriseDataObject(String refDataSetCode, String refDataCode) {
        RefDataObject edo = null;
        Map<String, MappedDataObject> refDataSet = ds.getRefDataSetMap(refDataSetCode);
        if (refDataSet == null) {
            logger.info("Unknown refDataSet: "+refDataSetCode);
        } else {
            MappedDataObject mdo = refDataSet.get(refDataCode);
            if (mdo == null) {
                logger.info("No refData with code "+refDataCode+" exists for refDataSet "+refDataSetCode);
            } else {
                edo = ds.getEnterpriseDataMap().get(mdo.getEnterpriseCode());
                if (edo == null) {
                    logger.info("No enterprise data exists with code: "+mdo.getCode());
                }
            }
        }
        return edo;
    }

    public RefDataObject retrieveRefDataObject(String refDataSetCode, String enterpriseCode) {
        RefDataObject rdo = null;
        Map<String, MappedDataObject> refDataSet = ds.getRefDataSetMap(refDataSetCode);
        for (Entry<String, MappedDataObject> entry : refDataSet.entrySet()) {
            if (entry.getValue().getEnterpriseCode().equals(enterpriseCode)) {
                rdo = RefDataUtil.createRefDataObject(entry.getValue().getCode(), 
                                                        entry.getValue().getName(), 
                                                        entry.getValue().getInfo());
                break;
            }
        }
        return rdo;
    }
}