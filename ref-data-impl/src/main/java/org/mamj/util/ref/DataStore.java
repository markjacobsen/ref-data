package org.mamj.util.ref;

import java.util.List;
import java.util.Map;

public interface DataStore {
    void setEnterpriseData(List<RefDataObject> enterpriseData) throws IllegalStateException;

    List<RefDataObject> getEnterprizeData();

    Map<String, RefDataObject> getEnterpriseDataMap();

    Map<String, Map<String, MappedDataObject>> getAllRefData();

    void addRefDataSet(String refDataSetCode, List<MappedDataObject> refDataSet);

    boolean isRefDataSetLoaded(String refDataSetCode);

    Map<String, MappedDataObject> getRefDataSetMap(String refDataSetCode);
}
