package org.mamj.util.ref;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.ParameterResolver;

public class RefDataParameterResolver implements ParameterResolver {
    /**
     * This method is only called by the framework if supportsParameter() previously returned true for the same ParameterContext and ExtensionContext. 
     * Returns the resolved argument for the parameter.
     */
    @Override
    public Object resolveParameter(ParameterContext parameterContext,
        ExtensionContext extensionContext) throws ParameterResolutionException {
            RefData refData = new RefDataImpl();
            refData.setName("RefDataTest instance");
            System.out.println("Creating RefDataImpl: "+refData.getName());
        return refData;
    }
    
    /**
     * Invoke for all the parameters and returns true if this resolver can resolve an argument for the parameter. 
     * If it returns true the parameter will be resolved using the method resolveParameter()
     */
    @Override
    public boolean supportsParameter(ParameterContext parameterContext,
        ExtensionContext extensionContext) throws ParameterResolutionException {
        return (parameterContext.getParameter().getType() == RefData.class);
    }
}
