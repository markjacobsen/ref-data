package org.mamj.util.ref;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockitoExtension.class)
public class DataStoreTest {
    @Test
    public void testEntepriseDataExceptions() {
        Assertions.assertThrows(IllegalStateException.class, () -> { 
            DataStore ds = new DataStoreImpl();
            ds.setEnterpriseData(null); 
        });

        List<RefDataObject> entData = new ArrayList<>();
        Assertions.assertThrows(IllegalStateException.class, () -> {
            DataStore ds = new DataStoreImpl();
            ds.setEnterpriseData(entData);
        });
    }
}
