package org.mamj.util.ref;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class RefDataObjectTest {
    private RefDataObject rdo;

    public RefDataObjectTest() {
        rdo = new RefDataObject();
        rdo.setCode("MI");
        rdo.setName("Michigan");
        rdo.setInfo("26th state in the US");
    }

    @Test
    @DisplayName("Test \"code\" property")
    void testCode() {
        assertEquals("MI", rdo.getCode());
    }

    @Test
    @DisplayName("Test \"name\" property")
    void testName() {
        assertEquals("Michigan", rdo.getName());
    }

    @Test
    @DisplayName("Test \"info\" property")
    void testInfo() {
        assertEquals("26th state in the US", rdo.getInfo());
    }
}