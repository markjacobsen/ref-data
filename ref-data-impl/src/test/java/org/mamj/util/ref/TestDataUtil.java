package org.mamj.util.ref;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class TestDataUtil {
    @Inject
    RefData refData;

    private TestDataUtil() {
        List<RefDataObject> enterpriseData = new ArrayList<>();
        enterpriseData.add(RefDataUtil.createRefDataObject("OH", "Ohio", "Something"));
        enterpriseData.add(RefDataUtil.createRefDataObject("MI", "Michigan", "Something"));
        enterpriseData.add(RefDataUtil.createRefDataObject("WI", "Wisconsin", "Something"));
        refData.loadEnterpriseData(enterpriseData);

        List<MappedDataObject> mappedSet1 = new ArrayList<>();
        mappedSet1.add(RefDataUtil.createMappedDataObject("OH", "1", "", ""));
        mappedSet1.add(RefDataUtil.createMappedDataObject("MI", "2", "", ""));
        mappedSet1.add(RefDataUtil.createMappedDataObject("WI", "3", "", ""));
        refData.mapRefDataSet("ms1", mappedSet1);

        List<MappedDataObject> mappedSet2 = new ArrayList<>();
        mappedSet2.add(RefDataUtil.createMappedDataObject("OH", "A", "", ""));
        mappedSet2.add(RefDataUtil.createMappedDataObject("MI", "B", "", ""));
        mappedSet2.add(RefDataUtil.createMappedDataObject("WI", "C", "", ""));
        refData.mapRefDataSet("ms2", mappedSet2);
    }

    /*
    public static TestDataUtil getInstance() {
        return this;
    }
    */
}
