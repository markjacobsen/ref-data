package org.mamj.util.ref;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MappedDataObjectTest {
    private RefDataObject rdo;
    private MappedDataObject mdo;

    public MappedDataObjectTest() {
        System.out.println("constructor");
        rdo = new RefDataObject();
        rdo.setCode("MI");
        rdo.setName("Michigan");
        rdo.setInfo("26th state in the US");

        mdo = new MappedDataObject();
        mdo.setEnterpriseCode("MI");
        mdo.setCode("26");
        mdo.setName("Mich");
        mdo.setInfo("Admin system state code for MI");
    }

    @Test
    @DisplayName("Test \"master data code\" property")
    void testMasterDataCode() {
        assertEquals("MI", mdo.getEnterpriseCode());
    }

    @Test
    @DisplayName("Test \"code\" property")
    void testCode() {
        assertEquals("26", mdo.getCode());
    }

    @Test
    @DisplayName("Test \"name\" property")
    void testName() {
        assertEquals("Mich", mdo.getName());
    }

    @Test
    @DisplayName("Test \"info\" property")
    void testInfo() {
        assertEquals("Admin system state code for MI", mdo.getInfo());
    }
}
